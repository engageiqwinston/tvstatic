<div class="logo_wrap">
    @if(Board::hasAdvertiserLogo())
    <h3 style="display: none;">Advertiser<br> logo</h3>
    <img id="logo_display_pyramid" src="/{!! Board::advertiserLogo() !!}" border="0"/>
    @else
    <h3>Advertiser<br /> logo</h3>
    <img id="logo_display_pyramid" style="display: none;" src="" border="0"/>
    @endif
</div>
