<div class="col-md-6">
    <a href="#"
       class="volume" id="volume"
       data-original-title="Enter Volume"
       data-type="text"
       data-pk="{!! Board::getAdvertiserPlacement() !!}"
    >
    {!! Board::advertiserVolume('Volume') !!}
    </a>
</div>
