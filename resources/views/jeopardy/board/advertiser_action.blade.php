<div class="vertical_navigation">
    <a class="add_adv" id="add_ad_{!! Board::getPublisherPlacement() !!}"
       data-col="{!! Board::getPublisherPlacement() !!}"
       data-pub-id="{!! Board::publisherId() !!}"
    > + </a>
    <br />
    <a class="prevad" id="prevad{!! Board::getPublisherPlacement() !!}">
          <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
    </a>
    <a class="nextad" id="nextad{!! Board::getPublisherPlacement() !!}">
          <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Previous</span>
     </a>
</div>
