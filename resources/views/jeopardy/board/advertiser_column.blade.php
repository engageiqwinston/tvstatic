<div class="slideshow vertical"
     data-cycle-slides="> .advertiser"
     data-cycle-fx="carousel"
     data-cycle-timeout="0"
     data-cycle-carousel-visible="{!! Board::visibleRows() !!}"
     data-cycle-carousel-vertical="true"
     data-allow-wrap="false"
     data-col="{!! Board::getPublisherPlacement() !!}"
     data-cycle-next="#nextad{!! Board::getPublisherPlacement() !!}"
     data-cycle-prev="#prevad{!! Board::getPublisherPlacement() !!}"
>
    <?php
        Board::setAdvertiserPlacement(1);
        Board::setCurrentAdvertisers();
    ?>
    @for($row = 1; $row <= Board::minRows(); $row++)
    <?php
    Board::setAdvertiser();
    ?>
    <div class="advertiser tile{!! Board::occupied() !!}{!! ($row > Board::visibleRows()) ? ' more' : '' !!}"
        data-advertiser-id="{!! Board::advertiserId() !!}"
        data-advertiser-name="{!! Board::advertiserName() !!}"
        data-publisher-id="{!! Board::publisherId() !!}"
        data-tile-placement="{!! Board::getAdvertiserPlacement() !!}"
    >
        <div class="advertiser-wrap">
            <div class="x" style="position: absolute;font-size: 15px;color: #00b33c;z-index: 99;right: 10px;cursor: pointer;border-radius: 50% !important; display:none"><i class="fa fa-trash" aria-hidden="true"></i></div>
            @include('jeopardy.board.advertiser_logo')
            @include('jeopardy.board.advertiser_name')
            <div class="stats_wrap">
                <div class="price_wrap">
                    @include('jeopardy.board.advertiser_price')
                </div>
                <div class="volume_wrap">
                    @include('jeopardy.board.advertiser_volume')
                </div>
                <div id="backdrop"></div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <?php
    Board::incrementAdvertiserPlacement();
    ?>
    @endfor
</div>
