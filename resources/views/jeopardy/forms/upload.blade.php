<form role="form" id="upload_logo_form" method="post" enctype="multipart/form-data">
{!! csrf_field() !!}
      <div class="row">
          <div class="col-md-12">
              <div class="form-group form-md-line-input" style="margin-bottom: 0">

                <div class="row fileupload-buttonbar">
                    <div class="col-md-12">
                        <a href="/jeopardy/logo/download_logo_folder">
                            <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-download-alt"></i>
                                <span>Download All</span>
                            </span>
                        </a>
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Add files...</span>
                            <input type="file" id="file" name="file" multiple="multiple" />
                        </span>
                        <button type="submit" class="btn btn-primary start" id="start_upload" style="display: none;">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <span id="fileupload-name"></span>
                        <span id="progress"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
