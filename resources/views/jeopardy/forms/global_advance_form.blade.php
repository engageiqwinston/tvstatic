<form role="form" method="post" class="global_advance_form" id="global_advance_form">

    <div class="form-bodyx">
        <div class="form-group form-md-line-input">
            <div class="row pushtop">
                <label for="form_control_1" class="col-md-6 control-label">Use Custom Value for Offer</label>
                <div class="col-md-3" style="padding-top: 18px;">
                    <input type="checkbox"
                           name="use_custom_offer" id="use_custom_offer"
                           class="make-switch{!! (BoardSetting::useCustomOffer()) ? ' checked' : '' !!}"
                           data-on-color="success"
                           data-off-color="warning"
                    >
                </div>
                <div class="col-md-3" style="padding-top: 18px;">
                    <div id="select_row"
                         class="btn-group"
                         style="display: {!! (BoardSetting::useCustomOffer() && !BoardSetting::isRowOfferExists(BoardSetting::getMaxRowsPublisher())) ? 'block' : 'none' !!};"
                     >
                        <button type="button" class="btn btn-success">Add Row</button>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            @for($list = 1; $list <= BoardSetting::getMaxRowsPublisher(); $list ++)
                            <li style="display: {!! (BoardSetting::isRowOfferExists($list))? 'none': 'block' !!}">
                                <a  id="row_{!! $list !!}" class="row_selection" data-row="{!! $list !!}" href="javascript:;"> Row {!! $list !!}</a>
                            </li>
                            @endfor
                        </ul>
                    </div>
            </div>
            </div>
        </div>

        <div id="custom_offer" style="display: {!! (BoardSetting::useCustomOffer()) ? 'block' : 'none' !!};">
            @for($list = 1; $list <= BoardSetting::getMaxRowsPublisher(); $list ++)
                @if(BoardSetting::isRowOfferExists($list))
                <div class="form-group form-md-line-input">
                    <div class="row pushtop">
                        <label for="form_control_1" class="col-md-6 control-label">
                            Row {!! $list !!}
                        </label>
                        <div class="col-md-4">
                            <input type="number"
                                   name="offer_row_{!! $list !!}" id="offer_row_{!! $list !!}"
                                   class="form-control row_offer"
                                   value="{!! BoardSetting::getRowOffer($list) !!}"
                                   required
                             >
                            <span class="help-block"></span>
                        </div>
                        <div class="col-md-2">
    	                    <a href="javascript:;" class="btn btn-icon-only purple delete_row" data-row="{!! $list !!}">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
            @endfor
        </div>
    </div>

</form>
