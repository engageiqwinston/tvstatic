<!-- Left and right controls -->
<a class="left carousel-control"
   href="#jeopardy-rotator"
   role="button"
   data-slide="prev"
   style="top: 50%;"
>
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control"
   href="#jeopardy-rotator"
   role="button"
   data-slide="next"
   style="top: 50%;"
>
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
