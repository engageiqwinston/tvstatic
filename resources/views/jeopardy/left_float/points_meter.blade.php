<div  class="col-md-12" id="meter_wrap">
    <div id="card" style="perspective: 400px; position: relative; overflow: hidden;">
        <div class="front" id="{!! (PointsMeter::getPercent() >= 100 && PointsMeter::getPercent() != 0) ? 'flip_front' : '' !!}">
            <div class="" id="" style="margin-bottom: 10px; overflow: hidden;">
                <div class="cover">
                    <div class="bar"></div>
                    <div class="separator"></div>
                    <div class="bar"></div>
					<div class="separator"></div>
					<div class="bar"></div>
					<div class="separator"></div>
					<div class="bar"></div>
					<div class="separator"></div>
					<div class="bar"></div>
				</div>
				<div class="percent_txt">
					<h3 id="percent" style="">{!! PointsMeter::getPercent() !!}%</h3>
					{!! (PointsMeter::getPercent() >= 100 && PointsMeter::getPercent() != 0) ? '<span class="pulsate">CLICK TO DRAW' : '<span>TILL NEXT DRAW' !!}</span>
				</div>
				<div class="skill">
					<div class="outer">
						<div class="inner">
							<div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="back" id="{!! (PointsMeter::getPercent() >= 100 && PointsMeter::getPercent() != 0) ? 'flip_back' : '' !!}">
			<div style="border: 3px solid #fff; width: 100%; min-height: 279px; background-color: #fff; padding: 0 20px 20px;">
			</div>
		</div>
    </div>
</div>
