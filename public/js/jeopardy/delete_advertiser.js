/**
 * delete advertiser by column
 */
$(document).ready(function() {
	$("div.x").on('click', function(){
		//if(!IsLoggedIn(['admin', 'publisher', 'super_user'])) return;
		//stop the carousel
		var carousel = Object.create(Carousel);
		carousel.stop();
		var tile = $(this).parents('.tile');
		var id = $(this).parent().parent().attr('data-advertiser-id');
		var url = '/jeopardy/advertiser/delete-advertiser';
		var name = $(this).parent().parent().attr('data-advertiser-name');
		var text;
		if(name != ''){
			text ="You will not be able to recover advertiser " + name + " and its related points/volume/price?";
		} else {
			text = "You will not be able to recover advertiser and its related points/volume/price?";
		}
		swal({
			title: "Are you sure to delete this advertiser? ",
	        text: text,
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        closeOnConfirm: false
		},  function(isConfirm){
				if(!isConfirm) return;
				//else submit true ajax
				console.log(tile);
				tile.find('.logo_wrap').find('h3').show();
				tile.find('.logo_wrap').find('img').hide();
				tile.find('.name_wrap').css('background-color', '#242f35');
				tile.find('.name_wrap').attr('data-color', '#242f35');
				tile.find('.name_wrap').find('#advertiser_name').text('ADVERTISER');
				tile.find('.stats_wrap').find('.price_wrap').find('#price').text('0.00');	
				tile.find('.stats_wrap').find('.volume_wrap').find('#volume').text('0.00'); 
				tile.find('.name_wrap').find('.tile_color').minicolors('destroy').val('#242f35').attr('data-defaultvalue', '#242f35');
				// Set form token, a laravel requirement
		        COMMON.ajaxCsrfSetup();
				$.ajax({
					url: url,
					type: 'POST',
					data: { id: id},
					success: function(msg){
						swal("Done!", name + " and its associated values is Deleted!", "success");
						tile.find('.name_wrap').attr('data-color', '#242f35');
						tile.find('.occupied').attr('data-advertiser-id', '');
						tile.find('.name_wrap').find('#advertiser_name').editable('destroy');
						tile.find('.stats_wrap').find('.price_wrap').find('#price').editable('destroy');	
						tile.find('.stats_wrap').find('.volume_wrap').find('#volume').editable('destroy'); 
						console.log(msg);
						ADVERTISER.enableEdit();
					},
					error: function(error){
						console.log(error);
					}
				});
			}
		);
	});
	var IsLoggedIn = function (roles) {
		// If inside iframe
		if( self != top) {
			// Check if its already login, if not a login modal will show.
			if(!parent.parent.modal()){
				return false;
			}
			// Already login
			else {
				// Check user has the ability to edit.
				if(!ROLES.hasAbility2Edit(roles)) {
					COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
					return false;
				}
			}
		}
		// If the page is not iframe.
		else {
			// Check user has the ability to edit.
			if(!ROLES.hasAbility2Edit(roles)) {
				COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
				return false;
			}
		}
		return true;
	}
	/**
	 * show hide delete button for advertisers
	 */
	$(".advertiser-wrap")
	.mouseover(function(){
       $(this).find('div.x').show();
    })
    .mouseout(function(){
       $(this).find('div.x').hide();
    })
    $(".x").hover(function(){
    	$(this).css('cursor','pointer').attr('title','click to delete this advertiser!');
    }, function() {
    	$(this).css('cursor','auto');
    })
});
	
	
	