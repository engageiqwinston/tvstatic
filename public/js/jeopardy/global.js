/*!
 * TvProject - EngageIq
 * Global setting object
 * All action, event related to global setting were handled here.

 @class GLOBAL
 **/
var GLOBAL = (function($) {
	/*
	 * Data before the updating.
	 * Use to compare old data and new data.
	 *
	 * @variable String
	 */ 
    var old_column_per_slide, old_row_per_publisher, old_advertiser_visible, old_publisher_max_value;

    var use_custom_offer = false;

	/*
	 * Flag to check if its clear to submit the form.
	 *
	 * @variable Bolean
	 */
	var _processSubmission = true;

	/*
	 * Document ready, event listener.
	 *
	 */
    $(function(){
		/*
         * global icon triggered
		 *
		 */
        $('#get_global').on('click',
            function (event) {
                event.preventDefault();
                old_row_per_publisher = $('#row_per_publisher').val();
				old_advertiser_visible = $('#advertiser_visible').val();
				old_column_per_slide = $('#column_per_slide').val();
            }
        );

		/*
		 * Validate
		 *
		 */
		$('#row_per_publisher, #advertiser_visible').on('change',
			function () {
				if(parseInt($('#advertiser_visible').val()) > parseInt($('#row_per_publisher').val())) {
					$(this).parents('.form-group').addClass('has-error');
					COMMON.message('Value should not exceed Min. Rows Per Publisher".', 'Num. Visible Advertisers Exceed.');
					_processSubmission = false;
				} else {
					$('#row_per_publisher, #advertiser_visible').parents('.form-group').removeClass('has-error');
					_processSubmission = true;
				}
			}
		);

		/*
         * Submit button triggered
		 *
		 */
		$('#global_form').submit(
            function (event) {
                event.preventDefault();
                var global_values = COMMON.getValues('global_form');
                var global_advance_values = COMMON.getValues('global_advance_form');
                $.extend(global_values, global_advance_values);
                // console.log(global_values);
                // return;
                if(_processSubmission) processSubmission(global_values, 'add');
				else COMMON.message('Please check the fields for error.', 'Submission Failed.');
            }
        );

    	$('.row_selection').on('click', function () {
    		$(this).parent().hide();
    		addRow2Edit($(this).data('row'));
    		if($('#custom_offer .row_offer').length == 10) $('#select_row').hide();
    	});

    	$('#custom_offer').on('click', '.delete_row', function () {
    		$(this).parents('.form-group').remove();
    		var row_num = $(this).attr('data-row');
    		$('#row_' + row_num).parent().show();

    		$.ajax({
    			data: {key: 'offer_row_' + row_num},
    			type: 'post',
    			url: '/jeopardy/global/delete',
    			success: function (result) {
    			},
    			error: function (jqXHR, textStatus, errorThrown) {
    				console.log(jqXHR.responseText);
    			}
    		});
    	});

    	addRow2Edit = function (row) {
    		var add_row_to_edit = ' <div class="form-group form-md-line-input">\
    					                <div class="row pushtop">\
    					                    <label for="form_control_1" class="col-md-6 control-label" style="font-size: 20px; margin-top: 18px;">Row ' + row + '</label>\
    					                    <div class="col-md-4">\
    					                        <input type="number"\
    					                               name="offer_row_' + row + '" id="offer_row_' + row + '"\
    					                               class="form-control row_offer"\
    					                               value=""\
    					                               required\
    					                         >\
    					                        <span class="help-block"></span>\
    					                    </div>\
    					                    <div class="col-md-2">\
    										<a href="javascript:;" class="btn btn-icon-only purple delete_row" data-row="' + row + '" style="margin-top: 15px;">\
                                                                            <i class="fa fa-times"></i>\
                                                                        </a>\
    					                    </div>\
    					                </div>\
    					            </div>';
    		$('#custom_offer').append(add_row_to_edit);
    	}
    });

	/*
	 * Process form submission.
	 *
	 * @param Object input
	 * @param String action
	 */
    var processSubmission = function (input, action) {
        COMMON.ajaxCsrfSetup();

        $.ajax({
			data: input,
			type: 'post',
			url: '/jeopardy/global/' + action,
			success: function (result) {
				addResult(action, result);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
			}
		});
	};

	/*
	 * Series of action after ajax call on adding data.
	 *
	 * @param String action
	 * @param Object result
	 */
	var addResult = function(action, result) {
		if(action == 'add') {
			console.log(result);
			$('#global').modal('hide');
			COMMON.message('You have successfully created or updated a global settings.', 'Global Settings');
			if(old_row_per_publisher != result.global.row_per_publisher ||
			   old_advertiser_visible != result.global.advertiser_visible ||
			   old_column_per_slide != result.global.column_per_slide ||
			   old_publisher_max_value != result.global.publisher_max_value
			  ) {
                    COMMON.reload();
			}
		}
	};

	/*
	 * Series of action after ajax call on getting data.
	 *
	 * @param String action
	 * @param Object result
	 */
	var getDataResult = function(action, result) {
		if(action == 'get-data')  {
			console.log(result);
			$.each(result, function( key, value ) {
				$('#global #' + key).val(value);
				if(key == 'column_per_slide') old_column_per_slide = value;
				if(key == 'advertiser_visible') old_advertiser_visible = value;
				if(key == 'row_per_publisher') old_row_per_publisher = value;
				if (key == 'publisher_max_value') old_publisher_max_value = value;
			});
		}
	}
	
    var iniSwitch = function () {
        $('.make-switch').bootstrapSwitch({
              size: "medium",
              state: use_custom_offer,
              onSwitchChange: function(event, state) {
                event.preventDefault();
                if(state) $('#custom_offer, #select_row').show('slow');
                else $('#custom_offer, #select_row').hide('slow');
              }
        });
    }
	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function() {
        },
        setUseCustomOffer: function ($use_custom_offer) {
            use_custom_offer = $use_custom_offer;
            iniSwitch();
        },
        useCustomOffer: function () {
            return use_custom_offer;
        }
    }

    return publicMethod;

})(jQuery);
