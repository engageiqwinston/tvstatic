/*!
 * TvProject - EngageIq
 * Roles object
 * All action, event related to roles were handled here.

 @class ROLES
 **/
var ROLES = (function($) {
	/*
	 * Consist of user roles.
	 *
	 * @variable Object
	 */
	var user_roles = {};

	/*
	 * Set the user_roles variable with data
	 *
	 * @param Object roles
	 */
	var _setRoles = function (roles) {
		user_roles = roles;
	};

	/*
	 * Check user has this role
	 *
	 * @param Sting role
	 */
	var _has = function (role) {
		if($.inArray(role, user_roles) != -1) {
			return true;
		}
		return false;
	}

	/*
	 * Check user has this ability to edit
	 *
	 * @param Array/Object roles
	 */
	var _hasAbility2Edit = function (roles) {
			var has_ability = false;
			$.each( roles, function( key, value ) {
				if(_has(value)) {
					has_ability = true;
				}
			});
			return has_ability;
	}

 	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function() {
        },
		setRoles: function(roles) {
			console.log(roles);
            _setRoles(roles);
			if(ROLES.has('super_user')) {
				$('#card_event_list').parent().show();
			} else {
				$('#card_event_list').parent().hide();
			}

		},
		getRoles: function() {
			return user_roles;
		},
		has: function(role) {
			return _has(role);
		},
		hasAbility2Edit: function (roles) {
			return _hasAbility2Edit (roles);
		}
    }

    return publicMethod;

})(jQuery);

ROLES.init();
