/**
 * delete publisher
 */

var Publisher = (function($) {
    var url = '/jeopardy/publisher/delete-publisher';
    var _delete = function() {
        $('div.xpublisher').on('click' , function() {
            //if(!IsLoggedIn(['admin', 'publisher', 'super_user'])) return;
            //stop the carousel
            //var carousel = Object.create(Carousel);
            //carousel.stop();
            var id = $(this).parent().parent().attr('data-publisher-id');
            var div = $(this).closest('div.publisher-wrap');
            if (id) {
                swal({
                    title: 'Are you sure to delete this Publisher?',
                    text: 'You will not be able to recover this Publisher',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it",
                    closeOnConfirm: false
                }, function(isConfirm) {
                    if (!isConfirm) return;
                    //submit tru ajax
                    // Set form token, a laravel requirement
                    COMMON.ajaxCsrfSetup();
                    $.ajax({
                        url: url,
                        data: {id:id},
                        type:'POST',
                        success: function(msg){
                            swal("Done!", "Deleted!", "success");
                            div.find("img").hide();
                            div.html('<h2 class="add_publisher">Publisher <br> Logo</h2>');
                            $("i#refresh_page").trigger('click');
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    })
                })
            }
        });
        var IsLoggedIn = function (roles) {
            // If inside iframe
            if( self != top) {
                // Check if its already login, if not a login modal will show.
                if(!parent.parent.modal()){
                    return false;
                }
                // Already login
                else {
                    // Check user has the ability to edit.
                    if(!ROLES.hasAbility2Edit(roles)) {
                        COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
                        return false;
                    }
                }
            }
            // If the page is not iframe.
            else {
                // Check user has the ability to edit.
                if(!ROLES.hasAbility2Edit(roles)) {
                    COMMON.message('You dont\'t have the ability to edit it.', 'Action Prohibited!');
                    return false;
                }
            }
            return true;
        }
    }
    /** show hide delete button for publishers **/
    $(".publisher-wrap")
    .mouseover(function(){
       $(this).find('div.xpublisher').show();
    })
    .mouseout(function(){
       $(this).find('div.xpublisher').hide();
    })
    $(".xpublisher").hover(function(){
        $(this).css('cursor','pointer').attr('title','click to delete this Publisher!');
    }, function() {
        $(this).css('cursor','auto');
    })
    return {
        delete: function() {
            _delete();
        }
    }

})(jQuery);
Publisher.delete();