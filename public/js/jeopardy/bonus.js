/*!
 * TvProject - EngageIq
 * Bonus/Card object
 * All action, event related to bonus/card were handled here.

 @class BUNOS
 @use Bootstrap datatables flip
 @use JQuery flip
 **/
var BONUS = (function($) {
	/*
	 * Flag to check if card is flipped
	 *
	 * @variable Bolean
	 */
	var isFlipped = false;
	var is_flipped = 0; // use in datatables

	/*
	 * Data of card and instance when card is flipped.
	 *
	 * @variable Object
	 */
	var card_count = 0;
	var flipped_card;
	var flipped_instance;
    var card_event_table;

	/*
	 * Points meter data.
	 *
	 */
	var points = 0, goal = 0, percent = 0;

	/*
	 * Document ready, event listener.
	 *
	 */
    $(function(){
		//Card event  icon is click, a modal with the table of card event will be shown.
        $('#card_event_list').click(function() {
            getAllBonusEvent();
        });

        $('.modal-card_event').on('click', '.delete_event', function() {
            //$(this).parents('tr').hide('slow');

            var table = $('table#card_event').DataTable();

            table.row( $(this).parents('tr') )
            .remove()
            .draw();
            var bunos_id = $(this).attr('data-id');
            $.ajax({
                data: {'id': bunos_id},
                type: 'post',
                url: '/jeopardy/points-meter/delete',
                success: function (result) {
                    $('#panel_' + bunos_id).remove();
                    if (!result.errors) {
                        COMMON.message('You have successfully deleted the bonus', 'Delete Bonus');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        });

        historyEditable();
    });

    var historyEditable = function () {

        COMMON.ajaxCsrfSetup();

        $('.outer_bunos_description')
        .on('click', function (e) {
			// Check if inside frame, functionality for checking user as logged in triggered.
			// parent.parent.stop();//check if user has big boobs, then if big, stop the carousel, otherwise, just stop
            checkIfLoggedIn($(this), ['super_user'], false, e);

        }).editable({
            mode: 'inline',
            url: '/jeopardy/points-meter/update-description',
            inputclass: 'outer_bunos_description',
            success: function(response, newValue) {
                console.log(response);
            }
        });

        $('.outer_bunos_title')
        .on('click', function (e) {
            e.preventDefault();
			// Check if inside frame, functionality for checking user as logged in triggered.
			// parent.parent.stop();//check if user has big boobs, then if big, stop the carousel, otherwise, just stop
            checkIfLoggedIn($(this), ['super_user'], true, e);

        })
        .editable({
            mode: 'inline',
            url: '/jeopardy/points-meter/update-title',
            inputclass: 'outer_bunos_title',
            success: function(response, newValue) {
                console.log(response);
            }
        });
    }

	/*
	 * Show pop up login modal if not login.
	 * If not inside iframe, login modal is not rquired.
	 *
	 * @param Object obj
	 */
	var checkIfLoggedIn = function (obj, roles, toggle, event) {
		// If inside iframe
		if( self != top) {
			// Check if its already login, if not a login modal will show.
			if(!parent.parent.modal()){
				// Disable x-editable if not logged in.
				disableEditable(obj);
                if(toggle) obj.parent().find('span.accordion-toggle').trigger('click');
                event.preventDefault();
			}
			// Already login
			else {
				// Check user has the ability to edit.
				if(ROLES.hasAbility2Edit(roles)) {
					// Enable x-editable.
					enableEditable(obj);
				} else {
					// Disable x-editable.
					disableEditable(obj);
                    if(toggle) obj.parent().find('span.accordion-toggle').trigger('click');
                    event.preventDefault();
				}
			}
		}
		// If the page is not iframe.
		else {
			// Check user has the ability to edit.
			if(ROLES.hasAbility2Edit(roles)) {
				// Enable x-editable if logged in.
				enableEditable(obj);
			} else {
				// Disable x-editable if not logged in.
				disableEditable(obj);
                if(toggle) obj.parent().find('span.accordion-toggle').trigger('click');
                event.preventDefault();
			}
		}
	}

	/*
	 * Enable x-editable.
	 *
	 * @param Object obj
	 */
	var enableEditable = function (obj) {
		if(obj.hasClass('editable') && obj.hasClass('editable-disabled')) {
			obj.editable('toggleDisabled');
		}
	}

	/*
	 * Disable x-editable.
	 *
	 * @param Object obj
	 */
	var disableEditable = function (obj) {
		if(obj.hasClass('editable') && !obj.hasClass('editable-disabled')) {
			obj.addClass('editable-disabled');
			obj.editable('toggleDisabled');
		}
	}

	/*
	 * Function to convert table to datatables.
	 *
	 */
    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                if(i == 3)   oTable.fnUpdate(is_flipped, nRow, i, false);
                else oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            is_flipped = jqTds[3].innerHTML;
            jqTds[0].innerHTML =  aData[0] ;
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            //jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '" style="width: 100% !important;">';
            jqTds[2].innerHTML = '<textarea class="form-control margin-0" value="' + aData[2] + '" style="width: 100% !important;">' + aData[2] + '</textarea>';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + jqTds[3].innerHTML + '">';
            jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
            jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);
            var jqTextArea = $('textarea', nRow);

            oTable.fnUpdate(aData[0], nRow, 0, false);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqTextArea[0].value, nRow, 2, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqTds = $('>td', nRow);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [20, 25, 50, -1],
                [20, 25, 50, "All"] // change per page values here
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 20,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': false,
                'targets': [0, 4, 5]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "desc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new').click(function (e) {
            e.preventDefault();

            console.log('new');

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;

                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            oTable.fnDeleteRow(nRow);

            // ajax call
            _delete(aData[0]);
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            nNew = false;

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                saveRow(oTable, nRow);
                nEditing = null;

                var aData = oTable.fnGetData(nRow);
                var values = {
                        'id' : aData[0],
                        'title' : aData[1],
                         'description' : aData[2],
                          'is_flipped' : aData[3]
                    };

                /*  Save using the bonus object */
                if(values.id) {
                    _update(values);
                } else {
                    _add(values, oTable, nRow);
                    card_count++;
                }

            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    }

	/*
	 * Function to update details from result of random retrieving of bonus/card.
	 *
	 * @param Object result
	 */
    var randomBonusUpdate = function (result) {
        flipped_card = result.card;
        flipped_instance = result.instance;

		points = result.points;
		goal = result.goal;
		percent = result.percent;
        // update card modal table
        $('#card_' + result._id).html(result._is_flipped);

        //COMMON.animateMeter(result.percent);
        $('#percent').text(percent + '%').parent().hide();
        $('#points_wrap').text(points);
        $('#goal_wrap a').text(goal);

        // COMMON.animateMeter(percent);

        var html = '';
            html += '<h3>' +  result.card.title + '</h3>';
            html += '<p>' +  result.card.description + '</p>';
        $('.back div').html(html);
    };

	/*
	 * Function to add the details of random card to bunos list under meter points display.
	 *
	 */
	 var _prependBunos = function () {

        var html ='';
            html += '<div class="panel panel-default">\
                        <div class="panel-heading">\
                            <h4 class="panel-title">\
                                <a href="#bonus_list_' + flipped_instance + '" \
                                   class="accordion-toggle accordion-toggle-styled collapsed" \
                                   data-parent="#accordion3"\
                                   data-toggle="collapse" \
                                   aria-expanded="false"\
                                >BONUS #' + flipped_instance + ' : ' + flipped_card.title + '</a>\
                            </h4>\
                        </div>\
                        <div class="panel-collapse collapse" id="bonus_list_' + flipped_instance + '" aria-expanded="false" style="height: 0px;">\
                            <div class="panel-body">\
                                <p>' + flipped_card.description + '</p>\
                            </div>\
                        </div>\
                    </div>';
        $('#bonus_list').prepend(html);
    };

	/*
	 * Reset all flip column in card table to zero when no cards are available for random retrieving.
	 * This to prepare cards for another round of random retrieving.
	 *
	 */
    var resetAllIsFlippedColumnInBonusTable = function () {
        $('.is_flipped_col').each(function(){
              $(this).html(0);
        });
    };

	/*
	 * Get all bonus/card event to display on card event table.
	 *
	 */
    var getAllBonusEvent = function () {
        COMMON.ajaxCsrfSetup();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/jeopardy/points-meter/all',

            success: function (result) {
                if (!result.errors) {
                    bonusEventTable(result);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

	/*
	 * Format return data of card event to html.
	 *
	 * @param Object result
	 */
    var bonusEventTable = function(result) {

		var option = {};
        var html = '';
        $('.bunos_description').editable('destroy');

        if(card_event_table) $('table#card_event').DataTable().destroy();

        $.each(result, function(index, value) {
                html += '<tr>';
                html += '  <td>' + value.id + '</td>';
                //html += '  <td>' + value.price + '</td>';
                html += '  <td >' + value.points + '</td>';
                html += '  <td>' + value.goal + '</td>';
                html += '  <td>' + value.card_flipped + '</td>';
                if(value.card) html += '  <td>' + value.card_id + '</td>';
                else  html += '  <td></td>';
                if(value.card) {
                    html += '   <td>';
                    html += '       <a href="#"';
                    html += '           class="bunos_title editable editable-pre-wrapped editable-click" id="bunos_title"';
                    html += '           data-original-title="Title"';
                    html += '           data-type="text"';
                    html += '           data-pk="' + value.id + '">';
                    html +=             value.card_title;
                    html += '       </a>';
                    html += '   </td>';
                }
                else  html += '  <td></td>';
                // if(value.card) html += '  <td>' + value.card.description + '</td>';
                if(value.card) {
                    html += '   <td>';
                    html += '       <a href="#"';
                    html += '           class="bunos_description editable editable-pre-wrapped editable-click" id="bunos_description"';
                    html += '           data-original-title="Description"';
                    html += '           data-type="textarea"';
                    html += '           data-pk="' + value.id + '">';
                    html +=             value.card_description;
                    html += '       </a>';
                    html += '   </td>';
                }
                else  html += '  <td></td>';
                html += '  <td>' + value.instance + '</td>';

                html += '  <td style="text-align: center;">';
                if(value.card) {
                    html += '     <i  data-id="' + value.id + '" class="btn btn-danger fa fa-trash delete_event"></i>';
                    html += '  </td>';
                }
                html += '</tr>';
        });
        $('#card_event tbody').html(html);

        // $('.bunos_description').editable();
        $('.bunos_description').editable({
            mode: 'inline',
            url: '/jeopardy/points-meter/update-description',
            inputclass: 'bunos_description',
            success: function(response, newValue) {
                // console.log($('#panel_' + response.id + ' .panel_title').find('a'));
                    $('#panel_' + response.id).find('a.outer_bunos_description').text(response.description).editable('destroy');
                console.log(response);
                historyEditable();
            }
        });

        $('.bunos_title').editable({
            mode: 'inline',
            url: '/jeopardy/points-meter/update-title',
            inputclass: 'bunos_title',
            success: function(response, newValue) {
                $('#panel_' + response.id).find('a.outer_bunos_title').text(response.title).editable('destroy');
                console.log(response);
                historyEditable();
            }
        });

        card_event_table = $('table#card_event')
            .dataTable({
                'order': [[ 0, "DESC" ]],
                'columnDefs': [ {
                    'targets': 8,
                    'orderable': false
                }]
            });
    };

	/*
	 * Initiate flip action.
	 *
	 * @param Bolean flip
	 * @param Bolean isFlipped
	 */
    var flip = function(flip, isFlipped) {
        if(flip == true) {
            var rotateAxis = "rotate" + "y";

            if(!isFlipped) {
                var side1 = 'front';
                var side2 = 'back';

                isFlipped = true;
            } else {
                var side2 = 'front';
                var side1 = 'back';

                isFlipped = false;
            }

            $('.' + side1).css({
              transform: rotateAxis + (false ? "(-180deg)" : "(180deg)"),
              "z-index": "0"
            });

            $('.' + side2).css({
              transform: rotateAxis + "(0deg)",
              "z-index": "1"
            });

            $('#card').effect("transfer",{ to: $("div#card_info_wrapper div") }, 500);
        }

        return isFlipped;
    };

	/*
	 * Add new bonus/card.
	 *
	 * @param Object input
	 * @param Object oTable
	 * @param Object nRow
	 */
    var _add = function (input, oTable, nRow) {
        COMMON.ajaxCsrfSetup();
        $.ajax({
            data: input,
            type: 'post',
            dataType: 'json',
            url: '/jeopardy/card/add',
            success: function (result) {
                if (!result.errors) {
                    if(oTable && nRow)  {
                        oTable.fnUpdate(result.card_id, nRow, 0, false);
                        oTable.fnUpdate(0, nRow, 3, false);
                    }
                    $('#get_cards').pulsate('destroy');
                    COMMON.message('You have successfully added a new bonus', 'Add Bonus');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

	/*
	 * Update existing bonus/card.
	 *
	 * @param Object input
	 */
    var _update = function (input) {
        COMMON.ajaxCsrfSetup();
        $.ajax({
            data: input,
            type: 'post',
            dataType: 'json',
            url: '/jeopardy/card/update',
            success: function (result) {
                if (!result.errors) {
                    $('#card_form').modal('hide');
                    COMMON.message('You have successfully updated the bonus', 'Edit Bonus');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

	/*
	 * Delete existing bonus/card.
	 *
	 * @param Integer id
	 */
    var _delete = function (id) {
        COMMON.ajaxCsrfSetup();
        $.ajax({
            data: {'id' : id},
            type: 'post',
            url: '/jeopardy/card/delete',
            success: function (result) {
                if (!result.errors) {
                    COMMON.message('You have successfully deleted the bonus', 'Delete Bonus');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

	/*
	 * Callback for function on getting a random bonus/card.
	 *
	 * @param Bolean flip
	 */
    var _getRandomBonus = function(flip) {
        COMMON.ajaxCsrfSetup();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/jeopardy/card/get-random-card',
            success: function (result) {
                if (!result.error) {
                    // get randon card
                    randomBonusUpdate(result);
					ADVERTISER.resetPoints();

                    COMMON.message('A bonus was flipped please check it out.', 'Flipped Bonus');
                } else {
                    resetAllIsFlippedColumnInBonusTable();
                    _getRandomBonus(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    };

	/*
	 * Series of action when flipping a card.
	 *
	 */
	var handleCardFlipping = function () {
		$('#card')
		.flip()
		.on('click', '#flip_front', function () {
			// Prevent the non super user to draw
			if(!ROLES.has('super_user_master')) {
				COMMON.message('Humans cannot draw the card.', 'Action Prohibited!');
				return;
			}

			if(card_count == 0) {
				COMMON.messageError('Please add bonuses first  inorder to draw. <br /> Click on the pulsating icon.', 'Add Bonuses First!');
				return;
            }

			// Get random card
			BONUS.getRandomBonus(true);
			// Series of effects
			$('#alert_container div').remove(); // remove notification
			$('.back').find('div').css({'color': '#000', 'background-color': '#fff'}); // change background color
			$('.percent_txt').hide(); // hide text, then
			$('#card').flip(true); // flip
		})
		.on('click', '#flip_back', function () {
			// Series of effects
			$_back = $(this);
			$('.percent_txt').find('span').html('TILL NEXT DRAW').attr('style', '')
							 .removeClass('pulsate').pulsate('destroy');
			$('#meter_wrap').effect("transfer",{ to: $("div#bonus_wrap") }, 1000,
				function(){
					BONUS.prependBunos();

   					$_back.find('div').css({'color': '#242f3', 'background-color': 'transparent'});
					$('#card').flip(false);
					$('#percent').text(percent + '%');

					setTimeout(function() {
						COMMON.animateMeter(percent);
					}, 800);

					// Remove id to disable flipping
					$('.front').attr('id', '');
					$('.back').attr('id', '');
				}
			);
		});
	};

	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
        init: function() {
            handleTable();
			handleCardFlipping();
        },

		setCardCount: function ($card_count)
		{
			card_count = $card_count;
		},

        getRandomBonus: function(flip) {
            _getRandomBonus(flip);
        },

        add: function (input,  oTable, nRow) {
            _add(input,  oTable, nRow);
        },

        update: function (input) {
            _update(input);
        },

        delete: function (id) {
            _delete(id);
        },

        prependBunos: function () {
            _prependBunos();
        }
    }

    return publicMethod;

})(jQuery);
