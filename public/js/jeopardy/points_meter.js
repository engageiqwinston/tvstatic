/*!
 * TvProject - EngageIq
 * Pointsmeter object
 * All action, event related to pointsmeter were handled here.

 @class POINTSMETER
 @use X-editable
 **/
var POINTSMETER = (function($) {
	/*
	 * Set gaol link to x-editable.
	 *
	 */
    var goalEditable = function () {
        $('.goal').on('click', function() {

			if(!ROLES.has('super_user')){
				if($(this).hasClass('editable') && !$(this).hasClass('editable-disabled')) {
					$(this).editable('toggleDisabled');
				}
				COMMON.message('You dont\'t have the ability to edit it.', 'Prohibited');
			}else {
				if($(this).hasClass('editable') && $(this).hasClass('editable-disabled')) {
					$(this).editable('toggleDisabled');
				}
			}
		}).editable({
            mode: 'inline',
            emptytext: '0',
            url: '/jeopardy/points-meter/pk-update',
            params: function(params) {
                params.points = $('#points_wrap').text();
                return params;
            },
            success: function(data, config) {
                 publicMethod.updatepointsMeter(data);
            },
            validate: function(value) {
               if(value < parseFloat($('#points_wrap').text())) return 'The gaol must be bigger in value than the points';
               if (!COMMON.isNumeric(value))   return 'Please use number and decimals only.';
            }
        }).on('shown', function(e, editable) {
            if(editable) {
                if (!COMMON.isNumeric(editable.input.$input.val())) {
                      editable.input.$input.val('00');
                }
            }
        }).on('hidden', function(e, reason){
			//
        });
    }

 	/*
	 * Public method.
	 *
	 */
    var publicMethod = {
		/*
		 * Initialization.
		 *
		 */
        init: function() {
            goalEditable();
        },

		/*
		 * Series of action to update points meter.
		 *
		 * @param Object data
		 */
        updatepointsMeter: function (data) {
            $('#percent').text(data.percent + '%').parent().hide();
            $('#points_wrap').text(data.points);
            $('#goal_wrap a').text(data.goal);

            if(parseFloat($('#points_wrap').text()) >= parseFloat($('#goal_wrap a').text())) {
                 //$('#goal_wrap a').pulsate({color: '#ed1a23', glow: true });
				 $('#percent').parent().find('span').html('CLICK TO DRAW.').addClass('pulsate').pulsate({color: '#ed1a23'});
            } else {
                $('#goal_wrap a').pulsate('destroy');
            }

            $('.skill .inner').animate({
                height:  data.percent + '%'
            }, 1500, function() {
                $('.percent_txt').fadeIn();
            });


            if(data.flipped)  {
                //$("#card").flip();
                //BONUS.getRandomBonus(true);
                $('.front').attr('id', 'flip_front');
                $('.back').attr('id', 'flip_back');
            }
        }
    }

    return publicMethod;

})(jQuery);

POINTSMETER.init();
