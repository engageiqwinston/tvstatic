<?php
namespace App\Jeopardy\Services\Message;
/**
 * the Messaging class for Jeopardy Module
 */

use Chikka;
use Config;
trait Message
{
	public function sendTextMessage()
	{
		Chikka::send(Config::get('chikkasenders.dashboard'),Config::get('chikkamessages.dashboard'));
	}
}