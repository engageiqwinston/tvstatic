<?php
namespace App\Jeopardy\Services;

use File;

use Illuminate\Support\Facades\Input;

final class Logo
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * Location of folder to scan.
     *
     */
    protected $dir;

    /**
     * Construct default.
     *
     */
    public function __construct($dir = 'logos')
    {
        $this->setDir($dir);
    }

    /**
     * Set the location directory.
     *
     * @param string $dir
     */
    public function setDir($dir)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $dir);

        $this->dir = $dir;
    }

    /**
     * Upload logo.
     *
     */
    public function upload()
    {
        if (Input::hasFile('file')) {
            $file = Input::file('file');
            Input::file('file')->move($this->dir, $file->getClientOriginalName());
            return pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            ;
        }
        return $_FILES;
    }

    /**
     * Download logo folder.
     *
     */
    public function downloadLogoFolder()
    {
        $zipFileName = 'logos.zip';

        $file_to_path = public_path() . '/logos/' . $zipFileName;
        $zip = new \ZipArchive;

        if ($zip->open( $file_to_path, \ZipArchive::CREATE ) === TRUE) {
            //$zip->addFile( public_path() .'/logos/' . 'ACOP.jpg' , 'ACOP.jpg');
            $files = \File::allFiles(public_path() . '/logos');

            foreach ($files as $file) {
                if(\File::extension($file) != 'zip') {
                    $file_name = \File::name($file) . '.' . \File::extension($file);
                    $zip->addFile( public_path() .'/logos/' . $file_name , $file_name);
                }
            }
            $zip->close();
        }

        echo header("Content-type: application/zip");
        echo header("Content-Disposition: attachment; filename=$zipFileName");
        echo header("Content-length: " . \File::size($file_to_path));
        echo header("Pragma: no-cache");
        echo header("Expires: 0");
        echo readfile("$file_to_path");
    }

    /**
     * Delete logo by path.
     *
     * @param object $data
     */
    public function delete($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        if (File::exists(public_path($data->path))) {
            File::delete(public_path($data->path));
        }
        return $data->path;
    }
}
