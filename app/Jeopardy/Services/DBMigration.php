<?php
namespace App\Jeopardy\Services;

use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use DB;
use Schema;

use App\Jeopardy\Entities\BoardSetting;

final class DBMigration
{
    public $data = array();

    public function __construct()
    {
    }

    public function refreshAll()
    {
        $this->migratePublisher();
        $this->migrateAdvertisers();
        $this->migrateCards();
        $this->migratePointsMeter();
        $this->migrateBoardSettings();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return Jeopardy</a></center>';
    }

    public function refreshPublishers()
    {
        echo "<center><h4>refreshPublishers</h4>";
        $this->migratePublisher();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return jeopardy</a></center>';
    }

    public function refreshAdvertisers()
    {
        $this->migrateAdvertisers();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return jeopardy</a></center>';
    }

    public function refreshCards()
    {
        $this->migrateCards();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return jeopardy</a></center>';
    }

    public function refreshPointsMeter()
    {
        $this->migratePointsMeter();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return jeopardy</a></center>';
    }

    public function refreshBoardSettings()
    {
        $this->migrateBoardSettings();

        echo "<center><h4>Done Rollback & Migrating...</h4>";

        echo '<a href="/jeopardy">Return jeopardy</a></center>';
    }

    private function migratePublisher()
    {
        if (Schema::hasTable('publishers')) {
            Schema::drop('publishers');
        }
        Schema::create('publishers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('publisher_name');
            $table->string('publisher_logo');
            $table->integer('tile_placement');
            $table->timestamps();
        });
    }

    private function migrateAdvertisers()
    {
        if (Schema::hasTable('advertisers')) {
            Schema::drop('advertisers');
        }
        Schema::create('advertisers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('publisher_id');
            $table->string('advertiser_name');
            $table->string('advertiser_logo');
            $table->string('color');
            $table->decimal('price');
            $table->integer('volume')->default(0);
            $table->decimal('points');
            $table->integer('tile_placement');
            $table->timestamps();
        });
    }

    private function migrateCards()
    {
        if (Schema::hasTable('cards')) {
            Schema::drop('cards');
        }
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('is_flipped')->default(0);
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    private function migratePointsMeter()
    {
        if (Schema::hasTable('points_meters')) {
            Schema::drop('points_meters');
        }
        Schema::create('points_meters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('points');
            $table->integer('goal')->default(0);
            $table->integer('card_flipped')->default(0);
            $table->integer('card_id')->default(0);
            $table->string('card_description');
            $table->integer('instance')->default(1);
            $table->timestamps();
        });
    }

    private function migrateBoardSettings()
    {
        if (Schema::hasTable('board_settings')) {
            Schema::drop('board_settings');
        }
        Schema::create('board_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publisher_id')->default(0);
            $table->string('scope')->default('global');
            $table->string('key')->unique();
            $table->string('value');
            $table->timestamps();
        });

        $setting = new BoardSetting;
        $setting->create([
            'scope' => 'global',
            'key' => 'global_goal_interval',
            'value' => 0,
            ])->create([
                'scope' => 'global',
                'key' => 'global_goal_1',
                'value' => 0,
            ])->create([
                'scope' => 'global',
                'key' => 'global_column_per_slide',
                'value' => 5,
            ])->create([
                'scope' => 'global',
                'key' => 'global_row_per_publisher',
                'value' => 4,
            ])->create([
                'scope' => 'global',
                'key' => 'global_advertiser_visible',
                'value' => 4,
            ]);
    }
}
