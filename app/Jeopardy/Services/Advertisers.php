<?php
namespace App\Jeopardy\Services;

use Board;
use Color;
use PointsMeter;
use BoardSetting;

final class Advertisers
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    static $staticAdvertiser;

    /**
     * instantiate and inject model
     *
     * @param Model $advertiser
     * @return void
     */
    public function __construct( \App\Jeopardy\Repositories\Contracts\AdvertiserRepository $advertiser )
    {
        self::$staticAdvertiser = $this->advertiser = $advertiser;
    }

    /**
     * Get all advertisers.
     *
     * @return array
     */
    public function getAll()
    {
        return $this->advertiser->all();
    }

    /**
     * Get all advertisers.
     *
     * @param String $group
     * @return array
     */
    public function getAllAndGroupBy($group)
    {
        // Validate arguments and throw errors
        $this->dataMust(['string' => true, 'error_id' => 'specify_group', 'recommended' => 'publisher_id'], $group);

        return $this->advertiser->getByGroup($group);
    }

    /**
     * Update advertiser by x-editable form.
     *
     * @param object $data
     * @return array
     */
    public function pkUpdate($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        $color = '#242f35'; $points = 0;
        extract($data->toArray());

        // Determine color
        Color::determineTheSelectedColor ($name, $color, $value);
        // If the color is change or set, update colors for same advertiser name
        if ($name == 'color' && (isset($advertisers) && $advertisers)) $this->advertiser->massUpdateColorByAdvertiserName($advertisers);

        // Check if advertiser exists
        if($this->advertiser->exists((int)$pk, (int)$publisher_id)){
            // Check whether the advertiser name is converted to engageiq or existing
            // If converted: new = true, add the whole points
            // If not: new = false, determine the difference of points, so
            // Set the data to update points meter | setPointsMeterData
            // getCurrentModel() the model was already filled with data when advertiser->exists() function was called.
            $new = $this->advertiser->isNew($name, PointsMeter::setPointsMeterData($this->advertiser->getCurrentModel(), $data->toArray()));
            // update advertiser
            $curr_advertiser = $this->advertiser->update($name, $value, $points);
        }
        // Advertiser dont exists
        else {
            $new = true;
            // Create new advertiser
            $curr_advertiser = $this->advertiser->create((int)$publisher_id, (int)$pk, $name, $value, $points);
        }

        // Update points
        $this->set2UpdatePoints($data->toArray(), $new);

        return array(
        //    'exists' => $this->advertiser->exists((int)$pk, (int)$publisher_id),
        //    'sub_points' => $points . ' ' . $this->advertiser->getCurrentModel()['points'],
        //    'updatepoints' => PointsMeter::checkIfNeeded2UpdatePointsMeter(),
        //    'addend' => PointsMeter::getAddend(),
           'errors' => false,
           'new' => $new,
           'advertiser' => $curr_advertiser,
           'advertiser_id' =>$this->advertiser->getID(),
           'points' => PointsMeter::getPoints(),
           'goal' => PointsMeter::getGoal(),
           'percent' => PointsMeter::getPercent(),
           'flipped' => PointsMeter::pointsExceedGoal()
        );
    }

    /**
     * Update advertisers points to 0.
     *
     * @return void
     */
    public function resetPoints()
    {
        $this->advertiser->resetAllNonEngageIqPoints();
    }

    /**
     * Set points for updating meter.
     *
     * @param array $data
     * @param bolean $new
     * @return void
     */
    protected function set2UpdatePoints($data, $new)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        if ($data['name'] == 'points' || $data['name'] == 'advertiser_name') {
            if ($new) {
                PointsMeter::create($this->setPointsMeter2Create($data));
                return;
            }

            if (PointsMeter::checkIfNeeded2UpdatePointsMeter()) {
                PointsMeter::updatePoints(PointsMeter::getAddend());
            }
        }
    }

    /**
     * Set points for updating meter.
     *
     * @param array $data
     * @return array
     */
    protected function setPointsMeter2Create($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        if ($data['name'] == 'points') {
            return ['points' => $data['value'], 'advertiser_name' => $data['advertiser_name']];
        }
        if ($data['name'] == 'advertiser_name') {
            return ['points' => (float)$data['points'], 'advertiser_name' => $data['value']];
        }
    }

    /**
     * Update advertisers points to 0.
     *
     * @return void
     */
    public static function addAllPointsOfEngageIq()
    {
        $advertiser = new \App\Jeopardy\Repositories\Eloquent\Advertiser(new \App\Jeopardy\Entities\Advertiser);

        return $advertiser->getSum('engageiq');
    }
    /**
     * delete advertiser
     * as requested by Jackie, april 25 2017 12:31 am
     * @resource
     */
    public function deleteAdvertiser()
    {
        $advertiserId = request()->input('id');
        $this->advertiser->deleteAdvertiserInAColumn($advertiserId);
        return "deleted";
    }
}
