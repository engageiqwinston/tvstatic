<?php
namespace App\Jeopardy\Services;

use PointsMeter;
use BoardSetting;

final class Publishers
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\ArrayHelper;
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    /**
     * instantiate and inject model
     *
     * @param Model $publisher
     * @return void
     */
    public function __construct(\App\Jeopardy\Repositories\Contracts\PublisherRepository $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * Get all publisher.
     *
     * @return array
     */
    final public function getAll()
    {
        return $this->fields2Index('tile_placement', $this->publisher->all());
    }

    /**
     * Get all publisher by id.
     *
     * @param integer $request
     * @return array
     */
    final public function getDataByID($id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $id);

        return $this->publisher->getData($id);
    }

    /**
     * set publisher by id if exist as active.
     *
     * @param integer $id
     * @return array
     */
    final public function setActivePublisher($id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $id);

        return $this->publisher->getData($this->getFirstNotEmpty($id));
    }

    /**
     * Add new publisher.
     *
     * @param object $data
     * @return array
     */
    final public function add($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        $this->publisher->save($data->toArray());

        return array(
                  'errors' => false,
                  'publisher' =>$this->publisher->getPublisherData(),
                );
    }

    /**
     * Update publisher.
     *
     * @param object $data
     * @return array
     */
    final public function update($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        $this->publisher->update($data->toArray());

        return array(
                  'errors' => false,
                  'publisher' =>$this->publisher->getPublisherData(),
                );
    }

    /**
     * Update publisher by placement.
     *
     * @param array $data
     * @return array
     */
    final public function updatePlacement($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $this->publisher->updatePlacement($data);

        return ['errors' => false];
    }

    /**
     * Delete publisher.
     *
     * @param object $data
     * @return array
     */
    final public function delete($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'object' => true], $data);

        $price = $this->publisher->getPriceByPlacement($data->tile_placement);
        $this->publisher->delete($data->tile_placement);

        return ['errors' => false];
    }
    /**
     * delete Publisher by id(id is preserve, only attibutes deleted)
     */
    public function deletePublisher()
    {
        $publisherId = request()->input('id');
        $this->publisher->deletePublisher($publisherId);
        return "{$publisherId} is deleted";
    }
}
