<?php
namespace App\Jeopardy\Services\Util;

use File;

use PointsMeter as Meter;
use BoardSetting;

class Board
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;

    protected $publishers, $last_publisher, $advertisers, $colors;
    protected $publisher_tile_placement;
    protected $advertiser_tile_placement;
    protected $publishers_min_rows = array();
    protected $columns, $ad_min_row, $vis_rows;
    protected $publishers_last_advertiser_placement = array();

	protected $default_column_per_slide = 5;
	protected $default_row_per_publisher = 4;
	protected $default_advertiser_visible = 4;

    protected $engageiq_color = '#00cc00';
	protected $default_color = '#242f35';


    /**
     * Instantiate
     * Set global settings variables
     *
     * @param Array $publisher
     */
	public function __construct ()
	{
		$settings = BoardSetting::getGlobalSettings();

		if(array_key_exists('global_column_per_slide', $settings)) $this->columns = $settings['global_column_per_slide']['value'];
		else $this->columns = $this->default_column_per_slide;

		if(array_key_exists('global_row_per_publisher', $settings)) $this->ad_min_row = $settings['global_row_per_publisher']['value'];
		else $this->ad_min_row = $this->default_row_per_publisher;

		if(array_key_exists('global_advertiser_visible', $settings)) $this->vis_rows = $settings['global_advertiser_visible']['value'];
		else $this->vis_rows = $this->default_advertiser_visible;
	}

    /**
     * Set all queried publisher to variable
     * Set the last publisher in array
     *
     * @param Array $publisher
     */
    public function setPublishers($publishers)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $publishers);

        $this->publishers = $publishers;
        if($this->hasPublishers()) {
            end($publishers);
            $this->last_publisher = $publishers[key($publishers)];
        }
    }

    /**
     * Set the placement of publisher, called upon for loop.
     *
     * @param Integer $publisher_tile_placement
     */
    public function setPublisherPlacement($publisher_tile_placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $publisher_tile_placement);

        $this->publisher_tile_placement = $publisher_tile_placement;
    }

    /**
     * Set the publisher min rows of advertiser in each column
     *
     */
    public function setPublisherMinrow()
    {
        $this->publishers_min_rows['pub' . $this->getPublisherPlacement()] = $this->minRows();
    }

    /**
     * Get the current publisher tile placement
     *
     * @return integer
     */
    public function getPublisherPlacement()
    {
        return $this->publisher_tile_placement;
    }

    /**
     * Incremation action upon for loop
     *
     */
    public function incrementPublisherPlacement()
    {
        $this->publisher_tile_placement++;
    }

    /**
     *  Set the placement of advertiser, called upon for loop.
     *
     * @param Integer $advertiser_tile_placement
     */
    public function setAdvertiserPlacement($advertiser_tile_placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $advertiser_tile_placement);

        $this->advertiser_tile_placement = $advertiser_tile_placement;
        //$this->current_advertiser = $this->getAdvertiser();
    }

    /**
     * Get the current advertiser tile placement
     *
     * @return Integer
     */
    public function getAdvertiserPlacement()
    {
        return $this->advertiser_tile_placement;
    }

    /**
     * Incremation action upon for loop
     *
     */
    public function incrementAdvertiserPlacement()
    {
        $this->advertiser_tile_placement++;
    }

    /**
     * Check if had a publishers
     *
     * @return Bolean
     */
    public function hasPublishers()
    {
        if($this->publishers)  return true;
        else return false;
    }

    /**
     * Check if the tile placement had a publisher assigned
     *
     * @return Bolean
     */
    public function hasPublisher()
    {
        if(array_key_exists($this->getPublisherPlacement(), $this->publishers)) {
            $this->current_publisher =  $this->publishers[$this->getPublisherPlacement()];
            return true;
        } else {
            $this->current_publisher = '';
            return false;
        }
    }

    /**
     * Setting our class $advertiser to the injected model
     *
     * @param Model $advertiser
     * @return EloquentRepository
     */
    public function getPublishers()
    {
        return $this->publishers;
    }

    /**
     * Get the current publisher upon the loop
     *
     * @return Array
     */
    protected function getPublisher()
    {
        return $this->current_publisher;
    }

    public function publisher()
    {
        return $this->getPublisher();
    }

    /**
     * Get the current publisher id upon the loop
     *
     * @return Integer
     */
    protected function getPublisherId()
    {
        return $this->current_publisher['id'];
    }

    public function publisherId()
    {
        return ($this->hasPublisher()) ? $this->getPublisherId() : '';
    }

    /**
     * Check if publisher have a logo assigned
	 *
	 * @return Bolean
     */
    public function hasPublisherLogo()
    {
        if($this->hasPublisher()) {
            if ($this->current_publisher['publisher_logo'] != '' &&
                File::exists(public_path('/' . $this->current_publisher['publisher_logo']))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the current publisher logo upon the loop
     *
     * @return String
     */
    protected function getPublisherLogo()
    {
        return $this->current_publisher['publisher_logo'];
    }

    public function publisherLogo()
    {
        return ($this->hasPublisher()) ? $this->getPublisherLogo() : '';
    }

    /**
     * Get the current publisher name upon the loop
     *
     * @return String
     */
    protected function getPublisherName()
    {
        return $this->current_publisher['publisher_name'];
    }

    public function publisherName()
    {
        return ($this->hasPublisher()) ? $this->getPublisherName() : '';
    }

    /**
     * Get the last publisher in the publishers array
     *
     * @return Array
     */
    public function getLastPublisherTilePlacement()
    {
        return $this->last_publisher['tile_placement'];
    }

    /**
     * Get publisher min rows of advertiser in each column
     *
     * @return Integer
     */
    public function publisherMinRows()
    {
        return $this->publishers_min_rows;
    }

    /**
     * Get the last publisher placement
     *
     * @return Integer
     */
    public function lastPublisherTilePlacement()
    {
        return ($this->hasPublishers()) ? $this->getLastPublisherTilePlacement() : 1;
    }

    /**
     * Set all queried advertisers to variable
     *
     * @param Array $advertisers
     */
    public function setAdvertisers($advertisers)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $advertisers);

        $this->advertisers = $advertisers;
    }

    /**
     * Get all advertisers
     *
     */
    public function getAllAdvertisers()
    {
        return $this->advertisers;
    }

    /**
     * Check if publishers had an advertisers
     *
     * @return Bolean
     */
    public function publisherHasAdvertiser()
    {
        if(array_key_exists($this->publisherId(), $this->advertisers)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set the current advertiser upon the loop
     *
     */
    public function setCurrentAdvertisers()
    {
        if($this->publisherHasAdvertiser()) {
            $this->current_advertisers =  $this->advertisers[$this->publisherId()];
        } else {
            $this->current_advertisers = array();
        }
    }

    /**
     * Get all current advertisers upon the loop
     *
	 * @return Array
     */
    public function getAdvertisers()
    {
        return $this->current_advertisers;
    }

    public function advertisers()
    {
        return $this->getAdvertisers();
    }

    /**
     * Set the current advertiser upon the advertisers loop
     * Set the last advertiser placement by publisher
	 *
     */
    public function setAdvertiser()
    {
        if($this->hasCurrentAdvertiser()) {
            $this->current_advertiser =  $this->current_advertisers[$this->getAdvertiserPlacement()];
        } else {
            $this->current_advertiser = '';
        }

		// Get the last advertiser placement and save to publishers_last_advertiser_placement
		// to have a record of last placement per columns.
        $this->publishers_last_advertiser_placement['pub' . $this->getPublisherPlacement()] = $this->getAdvertiserPlacement();
    }

    /**
     * Get the last advertiser placement per column/publisher
	 *
	 * @return Integer
     */
    public function getPublishersLastAdvertiserPlacement()
    {
        return $this->publishers_last_advertiser_placement;
    }

    /**
     * Check if placement have an advertiser assigned upon advertiser loop
	 *
	 * @return Bolean
     */
    public function hasCurrentAdvertiser()
    {
        if(array_key_exists($this->getAdvertiserPlacement(), $this->current_advertisers)) {
             return true;
        }else {
             return false;
        }
    }

    /**
     * Get the current advertiser upon loop
	 *
  	 * @return Array
     */
    protected function getAdvertiser()
    {
        return $this->current_advertiser;
    }

    public function advertiser()
    {
        return $this->getAdvertiser();
    }

    /**
     * Get the current advertiser id upon loop
	 *
	 * @return Integer
     */
    protected function getadvertiserId()
    {
        return $this->current_advertiser['id'];
    }

    public function advertiserId()
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserId() : '';
    }

    /**
     * Check if advertiser have a logo assigned
	 *
	 * @return Bolean
     */
    public function hasAdvertiserLogo()
    {
        if($this->hasAdvertiser()) {
            if (trim($this->current_advertiser['advertiser_logo']) && File::exists(public_path($this->current_advertiser['advertiser_logo']))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the current advertiser logo upon loop
	 *
	 * @return String
     */
    protected function getadvertiserLogo()
    {
        return $this->current_advertiser['advertiser_logo'];
    }

    public function advertiserLogo()
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserLogo() : '';
    }

    /**
     * Get the current advertiser name upon loop
	 *
	 * @return String $default
     */
    protected function getadvertiserName($default = '')
    {
        return ($name = $this->current_advertiser['advertiser_name']) ? strtoupper($name) : $default;
    }

    public function advertiserName($default = '')
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserName($default) : $default;
    }

    /**
     * Count the space in the string
	 *
	 * @return Integer
     */
    public function spaceCount()
    {
        return substr_count(trim($this->advertiserName('Advertiser')), ' ');
    }

    /**
     * count the space in the string
	 *
	 * @return String
     */
    public function longString()
    {
        return ($this->spaceCount() >= 2) ? ' long-str': '';
    }

    /**
    * Get the current advertiser color upon loop
	*
	* @return String $defaul
     */
    protected function getadvertiserColor($default)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'string' => true], $default);

        if ($color = $this->current_advertiser['color']) {
                if(strtolower($this->advertiserName()) == 'engageiq') return $this->engageiq_color;
                else return  $color;
        } else {
            return $default;
        }
    }

    /**
     * the default color for advertiser
     *
     * @return string
     */

    public function advertiserColor($color = '')
    {
        if(!$color) $color = $this->default_color;
        return ($this->hasAdvertiser()) ? $this->getadvertiserColor($color) : $color;
    }

    /**
    * Get the current advertiser price upon loop
	*
	* @return Decimal
     */
    protected function getadvertiserPrice()
    {
        return ($price = $this->current_advertiser['price']) ?  : 0.00;
    }

    public function advertiserPrice($default = '')
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserPrice() : $default;
    }

    /**
    * Get the current advertiser points upon loop
	*
	* @return Decimal
     */
    protected function getadvertiserPoints($default_points)
    {
        return (($points = $this->current_advertiser['points']) != 0) ? $points : number_format($default_points, 2, '.', '');
    }

    public function advertiserPoints($default_points = 0.00)
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserPoints($default_points) : number_format($default_points, 2, '.', '');
    }

    /**
    * Get the current advertiser volume upon loop
	*
	* @return Decimal
     */
    protected function getadvertiserVolume()
    {
        return ($volume = $this->current_advertiser['volume']) ? $volume : 00;
    }

    public function advertiserVolume($default = '')
    {
        return ($this->hasAdvertiser()) ? $this->getadvertiserVolume() : $default;
    }

    /**
    * Check if current advertiser is not empty
	*
	* @return Bolean
     */
    public function hasAdvertiser()
    {
        if($this->current_advertiser) return true;
        else return false;
    }

    /**
    * Get the number of slides from settings
	*
	* @return Integer
     */
    public function numberOfSlides ()
    {
        $columns = $this->columns;
        if(!$columns) $columns = 4;

        $publishers = $this->publishers;
        end($publishers);
        $number_of_slides = ceil(key($publishers) / $columns);
        if(!$number_of_slides) $number_of_slides =  1;

        return  $number_of_slides;
    }

    /**
    * Get the column per slide from settings
	*
	* @return Integer
     */
    public function columnPerSlide()
    {
        $columns = $this->columns;
        if($columns  < 2) $columns = $this->default_column_per_slide;
        return $columns;
    }

    /**
    * Get the minimum rows per column/publisher from settings
	*
	* @return Integer
     */
    public function minRows()
    {
        $ad_min_row = $default_max = $this->ad_min_row;
        if($this->hasPublisher()) {
            if($this->publisherHasAdvertiser()) {
                 $advertisers = $this->current_advertisers;
                 end($advertisers);
                 if(($ad_min_row = key($advertisers)) < $default_max) $ad_min_row = $default_max;
            }
        }
        if($ad_min_row  < 5) $ad_min_row = $this->default_row_per_publisher;
        return $ad_min_row;
    }

    /**
    * Get the number of visible rows per column/publisher from settings
	*
	* @return Integer
     */
    public function visibleRows()
    {
        $vis_rows = $this->vis_rows;
        if($vis_rows  < 2) $vis_rows = $this->default_advertiser_visible;
        return $vis_rows;
    }

    /**
    * Get the maximum height of column in relation to visible rows
	*
	* @return Integer
     */
    public function maxHeight()
    {
        $num_box_visible = Board::visibleRows();
        $portion = 1/$num_box_visible;
        $height_box_visible =  171 + $portion; // css style height in px
        $publisher_box_height = 80; // css style height in px
        $nav_height = 49; // css style height in px
        $max_height = ($num_box_visible * $height_box_visible) + $publisher_box_height + $nav_height;
        return $max_height;
    }

    /**
    * Check if the placement is have an advertiser assigned to it.
	*
	* @return String - Class name
     */
    public function occupied()
    {
        return ($this->hasAdvertiser()) ? ' occupied': '';
    }
}
?>
