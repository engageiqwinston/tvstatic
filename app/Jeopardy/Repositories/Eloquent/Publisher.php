<?php

namespace App\Jeopardy\Repositories\Eloquent;

use Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Jeopardy\Services\Mail\Email as Email;

class Publisher implements \App\Jeopardy\Repositories\Contracts\PublisherRepository
{
    /**
     * Traits.
     *
     */
    use \App\Jeopardy\Repositories\Eloquent\Helpers\EloquentHelper;
    use \App\Jeopardy\Services\Helpers\DataHelper;
    use \App\Jeopardy\Services\Helpers\ExceptionHelper;
    use Email;

    /**
     * Our Eloquent card model.
     *
     * @var object
     */
    protected $model;

    /**
     *  Publisher data.
     *
     * @var object
     */
    protected $publisher;

    /**
     * Format for return.
     *
     * @var string
     */
    protected $return = 'array';
    
    /**
     * website's folder name use in copying files
     */
    private $website;

    /**
    * Setting our class $publisher to the injected model
    *
    * @param Model $publisher
    * @return EloquentRepository
    */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->website = getenv('WEBSITE_FOLDER');
    }

    /**
     * Get all publishers.
     *
     * @return mixed
     */
    public function getPublisherData()
    {
        return $this->format($this->publisher);
    }

    /**
     * Get all publishers.
     *
     * @return mixed
     */
    public function all()
    {
        //return $this->model->orderBy('tile_placement', 'ASC')->with('advertisers')->get();
        return $this->format($this->model->orderBy('tile_placement', 'ASC')->get());
    }

    /**
     * Save publisher.
     *
     * @param array $data
     * @return void
     */
    public function save($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $this->publisher = $this->model;
        $this->publisher->publisher_name = $data['publisher_name'];
        $this->publisher->publisher_logo = $data['publisher_logo'];
        $this->publisher->tile_placement = $data['tile_placement'];
        $this->publisher->save();
        try {
            copy(public_path() . '/'.$data['publisher_logo'], $this->website.'/'.$data['publisher_logo']);
        } catch (Exception $e) {
            echo $e;
        }    
    }

    /**
     * Update publisher.
     *
     * @param array $data
     * @return void
     */
    public function update($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $publisher = $this->model->where(array('tile_placement' => $data['tile_placement']))->first();
        $this->publisher = $publisher;

        $this->publisher->publisher_name = $data['publisher_name'];
        $this->publisher->publisher_logo = $data['publisher_logo'];
        $this->publisher->tile_placement = $data['tile_placement'];
        $this->publisher->push();
        try {
            copy(public_path() . '/'.$data['publisher_logo'], $this->website.'/'.$data['publisher_logo']);
        } catch (Exception $e) {
            echo $e;
        }
    }

    /**
     * Update publisher tile placement.
     *
     * @param array $data
     * @return void
     */
    public function updatePlacement($data)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'array' => true], $data);

        $this->publisher = $this->model->where(array('id' => $data['id']))->first();
        $this->publisher->tile_placement = $data['placement'];
        $this->publisher->push();
    }

    /**
     * Delete publisher.
     *
     * @param integer $placement
     * @return void
     */
    public function delete($placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $placement);

        $this->model->where(array('tile_placement' => $placement))->delete();
    }

    /**
     * Get publisher data by id.
     *
     * @param integer $id
     * @return array
     */
    public function getData($id)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $id);

        $this->publisher = $this->model->where(array('id' => $id))->first();
        return $this->format($this->publisher);
    }

    /**
     * Get all publisher data by placement.
     *
     * @param integer $placement
     * @return mixed
     */
    public function dataByPlacement($placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $placement);

        $this->publisher = $this->model->where(array('tile_placement' => $placement))->get();
        return $this->format($this->publisher);
    }

    /**
     * Get price by placement.
     *
     * @param integer $placement
     * @return mixed
     */
    public function getPriceByPlacement($placement)
    {
        // Validate arguments and throw errors
        $this->dataMust(['required' => true, 'integer' => true], $placement);

        $publisher = $this->dataByPlacement($placement);

        return $publisher[0]['price'];
    }
    /**
     * delete publisher logo and publisher name
     * publisher id and tile placement is preserve
     * @var resource
     */
    public function deletePublisher($publisherId)
    {
        $this->publisher = $this->model->findOrFail($publisherId);
        /**
         * check if publisher's related advertisers is empty
         * if true, delete the publisher
         * @var resource
         * return boolean
         */
        if ($this->publisher->has('advertisers', '<', 1)) {
            $this->publisher->find($publisherId)->delete();
        } else {
            $this->publisher->publisher_name = "";
            $this->publisher->publisher_logo = "";
            $this->publisher->save();
        }
    }
    /**
     * delete publishers if value is 0 tile_placement
     */
    public function deleteZeroValueTilePlacement()
    {
        $this->publisher = $this->model->where('tile_placement', '0')->delete();
        $this->deletePublisherNotification();
    }
}
