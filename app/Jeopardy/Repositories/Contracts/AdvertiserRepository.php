<?php

namespace App\Jeopardy\Repositories\Contracts;

/**
 * An interface....
 */
interface AdvertiserRepository
{

    public function getAdvertiserData();

    public function getData($id);

    public function getByColor($color);

    public function get();

    public function getByGroup($group);

    public function getBy($param);

    public function all();

    public function exists($tile_placement, $publisher_id);

    public function massUpdateColorByAdvertiserName($advertiser_name);

    public function update($name, $value, $points = '');

    public function create($publisher_id, $tile_placement, $name, $value, $points = '');

    public function dataByPlacement($placement);

    public function getPriceByPlacement($placement);

	public function resetAllPoints();

	public function resetAllEngageIqPoints();

	public function resetAllNonEngageIqPoints();

    public function deleteAdvertiserInAColumn($advertiserId);
}
