<?php

namespace App\Jeopardy\Repositories\Contracts;

/**
 * An interface....
 */
interface SettingRepository
{
    public function all();

    public function where($where);

    public function getByScope($id);

    public function getByKey($key);

    public function add($data, $scope);

}
