<?php

namespace App\Jeopardy\Repositories\Contracts;

/**
 * An interface....
 */
interface PointsMeterRepository
{
    public function select($array);

    public function all();

    public function add($where, $data);

    public function updateOrCreate($data);

    public function updatePoints($addend, $obj = '');

    public function updatePointsBrute($total);

    public function updateCardflipped($card);

    public function getActive($return_array = true);
}
