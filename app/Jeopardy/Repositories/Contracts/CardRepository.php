<?php

namespace App\Jeopardy\Repositories\Contracts;

/**
 * An interface....
 */
interface CardRepository
{
    public function all();

    public function add($data);

    public function update($data);

    public function delete($id);

    public function randonCard();
}
