<?php

namespace App\Http\Controllers;

use Auth;
use Board;
use Color;
use Config;
use PointsMeter;
use BoardSetting;
use CardsAndEvents;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Traits\HasRolesTrait as HasRoles;

class JeopardyController extends Controller
{
    /**
     * Traits.
     *
     */
    use HasRoles;

    /**
     * Injected Class.
     * app\Jeopardy\Services
     *
     * @var object $card, $logo, $meter, $publisher;
     */
    protected $card;
    protected $logo;
    protected $meter;
    protected $publisher;

    /**
     * List of methods used for injected class to call.
     * config/Jeopardy.php
     *
     * @var array
     */
    protected $methods = [
        'all' => 'selectAll',
        'refresh_all' => 'refreshAll',
        'refresh_publishers' => 'refreshPublishers',
        'refresh_advertisers' => 'refreshAdvertisers',
        'refresh_cards' => 'refreshCards',
        'refresh_points_meter' => 'refreshPointsMeter',
        'refresh_board_settings' => 'refreshBoardSettings',
        'update-placement' => 'updatePlacement',
        'get-data' => 'getDataByID',
        'get-random-card' => 'getRandomCard',
        'get-publisher' => 'getByPublisherID',
        'get-by-publisher-id' => 'getByPublisherID',
        'get-data-global' => 'getGlobalSettings',
        'add-global' => 'addGlobal',
        'delete-global' => 'deleteGlobal',
        'pk-update' => 'pkUpdate',
        'pk-update-color' => 'pkUpdateColor',
        'download_logo_folder' => 'downloadLogoFolder',
        'update-title' => 'updateTitle',
        'update-description' => 'updateDescription',
        'delete-advertiser' => 'deleteAdvertiser',
        'delete-publisher' => 'deletePublisher'
    ];

    /**
     * Instantiate ...
     *
     * @param  object  $publisher
     * @param  object  $advertisers
     * @param  object  $card
     * @param  object  $logo
     * @return void
     */
    public function __construct (
        \App\Jeopardy\Services\Publishers $publisher,
        \App\Jeopardy\Services\Advertisers $advertiser,
        \App\Jeopardy\Services\Cards $card,
        \App\Jeopardy\Services\Logo $logo
        )
    {
        $this->card = $card;
        $this->logo = $logo;
        $this->publisher = $publisher;
        $this->advertiser = $advertiser;

        // Check if user has roles and get.
        $this->CheckIfUserHasRoles();
        // Share the data to view
		view()->share('title', 'Jeopardy');
		view()->share('roles', $this->user_roles);
    }

    /**
     * Check if method exist on the method list
     *
     * @return string
     */
    protected function checkMethod($method) {
        return (array_key_exists($method, $this->methods)) ? $this->methods[$method] : $method;
    }

    /**
     * Display board
     *
     * @return view
     */
    public function index ()
    {
        // Get all publishers
        Board::setPublishers($this->publisher->getAll());

        // Get all advertiser
        Board::setAdvertisers($this->advertiser->getAllAndGroupBy('publisher_id'));

        // Set colors in relation to advertiser name
        Color::setColors(['engageiq_color' => '#00cc00', 'default_color' => '#242f35']);

        // Get all cards for table
        CardsAndEvents::setCards($this->card->selectAll());

        // Get all cards events for table
        CardsAndEvents::setCardsEvents($this->card->getCardEvents());
        
        return view('jeopardy.index');
    }

    /**
     * Scan the folder for logos to display in modal
     *
     * @param  object  $scan
     * @return array/json
     */
    public function scan(\App\Jeopardy\Services\ScanFile $scan)
    {
        $scan->run();
    }

    /**
     * Card methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function card (Request $request, $method)
    {
		// Reset points of all engageiqadvertiser when reach goal/card flipping.
		if($method == 'get-random-card') $this->advertiser->resetPoints();

        return $this->card->{$this->checkMethod($method)}($request);
    }

    /**
     * Publisher methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function publisher (Request $request, $method)
    {
        return $this->publisher->{$this->checkMethod($method)}($request);
    }

    /**
     * Advertiser methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function advertiser (Request $request, $method)
    {
        return $this->advertiser->{$this->checkMethod($method)}($request);
    }

    /**
     * Global Settings methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function globalSetting (Request $request, $method)
    {
        return BoardSetting::{$this->checkMethod($method . '-global')}($request);
    }

    /**
     * Points meter methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function pointsMeter (Request $request, $method)
    {
        return PointsMeter::{$this->checkMethod($method)}($request);
    }

    /**
     * Logo methods
     *
     * @return array/json
     */
    public function logo (Request $request, $method)
    {
        return $this->logo->{$this->checkMethod($method)}($request);
    }

    /**
     * Test methods, This is where I test the function of a certian class.
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function test (\App\Jeopardy\Services\Test $test, Request $request, $method)
    {
        $return = $test->{$this->checkMethod($method)}($request);

        if ($request->isMethod('get') && $return->view === true) return view('Jeopardy.test.' . $this->checkMethod($method), $test->data);
    }

    /**
     * Migration methods
     *
     * @param  array  $request
     * @param  string  $method
     * @return array/json
     */
    public function migration (\App\Jeopardy\Services\DBMigration $migration, Request $request, $method)
    {
        $migration->{$this->checkMethod($method)}($request);
    }
    public function getCardReachedGoal()
    {
        return PointsMeter::getCardReachedGoal();
    }
}
