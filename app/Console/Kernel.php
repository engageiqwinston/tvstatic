<?php

namespace App\Console;

use App\Exceptions\Handler;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Components\Feedback\FetchFeedback::class,
    ];

    /**
     * The Artisan commands that are scheduled to run on a certain frequency.
     * 
     * @var array
     */
    protected $scheduled = [
        \App\Components\Feedback\FecthFeedback::class => 'everyMinute',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        foreach ($this->scheduled as $command => $frequency) {
            try {
                $schedule->command($command)->$frequency();
            } catch (Exception $e) {
                $this->app->make(Handler::class)->report($e);
            }
        }
    }
}
