<?php

namespace App\Src\Widgets\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admins';
}
