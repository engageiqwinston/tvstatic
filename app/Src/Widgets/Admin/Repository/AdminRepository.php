<?php namespace App\Src\Widgets\Admin\Repository;

use App\Src\Widgets\Admin\Model\Admin;

class AdminRepository
{
    private $admin;
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }
    public function getAllAdminDataLimit10()
    {
        return $this->admin->orderBy('created_at', 'desc')->limit(50)->get();
    }
}
