<?php

// namespace Tests\Unit;

// use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminWidgetTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->SUT = new App\Src\Widgets\Admin\Repository\AdminRepository(new App\Src\Widgets\Admin\Model\Admin);
    }

    /**
     * @test
     */
    public function getAllAdminDataLimit10()
    {
        $data = $this->SUT->getAllAdminDataLimit10();
        var_dump($data);
        $this->assertInternalType('object', $data);
    }
}
